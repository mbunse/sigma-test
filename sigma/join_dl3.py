import os.path
from fact.io import read_data
from sigma import CONFIG # import from this project
from urllib.request import urlretrieve

def __check_df(df, dl3_columns, reconstructed_columns=None):
    missing_id = set(CONFIG['join_id']) - set(df.columns)
    if len(missing_id) > 0:
        raise ValueError(f'df misses the following ID columns to join with: {list(missing_id)}')
    dl3_reconstruction = set(dl3_columns) - set(CONFIG['join_id']) # remove CONFIG['join_id'] from reconstructed columns
    df_reconstruction = set(df.columns) & dl3_reconstruction
    if len(df_reconstruction) == 0:
        raise ValueError(f'df misses one of the reconstruction columns: {list(dl3_reconstruction)}')
    df_unused = set(df.columns) - set(dl3_columns)
    if len(df_unused) > 0:
        raise ValueError(f'df contains unused columns: {list(df_unused)}')
    if reconstructed_columns is not None:
        missing_reconstruction = set(reconstructed_columns) - set(df_reconstruction)
        if len(missing_reconstruction) > 0:
            raise ValueError(f'df does not contain specified columns: {list(missing_reconstruction)}')

def __get_crab_dl3(crab_dl3_path):
    if not os.path.isfile(crab_dl3_path):
        print(f"Downloading {CONFIG['crab_dl3_url']} to {crab_dl3_path}")
        urlretrieve(CONFIG['crab_dl3_url'], crab_dl3_path) # download if not existing
    return read_data(crab_dl3_path, key=CONFIG['event_key'])

def join_dl3(df, reconstructed_columns=None, crab_dl3_path=None, verbose=False):
    """Join a custom reconstruction with the standard DL3 representation of the Crab nebula data. The
    joined data is returned as a dictionary with the keys sigma.CONFIG['event_key'] and
    sigma.CONFIG['run_key'], which resembles the structure of FACT's HDF5 files.

    The input DataFrame might be obtained from a CSV using pandas, e.g.:

        df = pd.read_csv(path, sep=',', names=sigma.CONFIG['event_key']+['gamma_prediction'])

    You can inspect all possible reconstructions that might be replaced (like 'gamma_prediction' or
    'gamma_energy_prediction') as follows:

        sigma.join_dl3.list_feasible_reconstructions()

    You can create an HDF5 file from the return value of this function with fact.io:

        fact.io.write_data(result[CONFIG['event_key']], output_path, key=CONFIG['event_key'])
        fact.io.write_data(result[CONFIG['run_key']], output_path, key=CONFIG['run_key'], mode='a')
    """
    if crab_dl3_path is None:
        crab_dl3_path = CONFIG['crab_dl3']
    crab_dl3 = __get_crab_dl3(crab_dl3_path)

    __check_df(df, crab_dl3.columns, reconstructed_columns)
    if reconstructed_columns is None:
        reconstructed_columns = list(set(df.columns) - set(CONFIG['join_id']))

    if verbose:
        print(f'Merging reconstructed_columns={reconstructed_columns} into the existing DL3 data')
    crab_dl3.drop(columns=reconstructed_columns, inplace=True)
    joined_df = crab_dl3.merge(
        df,
        on=CONFIG['join_id'],
        how='outer',
        indicator=True,
        validate='one_to_one'
    )
    if verbose:
        print('Both: ', len(joined_df[joined_df._merge == "both"]) / len(crab_dl3)*100, "% of crab set")
        print('Input-only: ', len(joined_df[joined_df._merge == "right_only"]) / len(crab_dl3)*100, "% of crab set")
        print('DL3-only: ', len(joined_df[joined_df._merge == "left_only"]) / len(crab_dl3)*100, "% of crab set")

    return {
        CONFIG['event_key']: joined_df,
        CONFIG['run_key']: read_data(crab_dl3_path, key='runs')
    }

def list_feasible_reconstructions(crab_dl3_path=None):
    """Return a list with all feasible reconstructions that can be replaced in the standard DL3
    representation of the Crab nebula."""
    crab_dl3 = __get_crab_dl3(CONFIG['crab_dl3'] if crab_dl3_path is None else crab_dl3_path)
    return list(set(crab_dl3.columns) - set(CONFIG['join_id']))
