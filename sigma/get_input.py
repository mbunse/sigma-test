import h5py
import os.path
import numpy as np
import pandas as pd
from fact.io import read_data
from sigma import CONFIG # import from this project
from urllib.request import urlretrieve

def get_crab_dl2(
        crab_dl2_path=None,
        all_features=False,
        only_id=False,
        n_samples=None,
        verbose=False):
    """Get the DL2 data to be predicted."""
    if only_id:
        features = CONFIG['join_id']
    elif all_features:
        features = None
    else:
        features = CONFIG['features']
    return __get_dl2(
        crab_dl2_path,
        'crab_dl2',
        'crab_dl2_url',
        features,
        n_samples,
        verbose
    )

def get_simulated_dl2(
        gamma_dl2_path=None,
        hadron_dl2_path=None,
        only_gamma=False,
        all_features=False,
        n_samples=None,
        gamma_diffuse=True,
        verbose=False):
    """Get the DL2 data to learn from."""
    features = None if all_features else CONFIG['features']
    dl2_args = (features, n_samples, verbose)
    if gamma_diffuse:
        dl2_props = ('gamma_diffuse_dl2', 'gamma_diffuse_dl2_url')
    else:
        dl2_props = ('gamma_dl2', 'gamma_dl2_url')
    gamma = __get_dl2(gamma_dl2_path, *dl2_props, *dl2_args)
    if only_gamma:
        return gamma
    hadron = __get_dl2(hadron_dl2_path, 'hadron_dl2', 'hadron_dl2_url', *dl2_args)
    gamma['class'] = 1
    hadron['class'] = 0
    return pd.concat([gamma, hadron])

def __get_dl2(path, path_property, url_property, features, n_samples, verbose):
    if path is None:
        path = CONFIG[path_property]
    if not os.path.isfile(path):
        print(f"Downloading {CONFIG[url_property]} to {path}")
        urlretrieve(CONFIG[url_property], path) # download if not existing
    if verbose:
        print(f"Reading {path}")
    df = read_data(path, key=CONFIG['event_key'], last=n_samples)
    df["area"] = df["width"] * df["length"] * np.pi
    df["log_size"] = np.log(df["size"])
    df["size_area"] = df["size"] / df["area"]
    df["area_size_cut_var"] = df["area"] / np.log(df["size"])**2
    return df if features is None else df[features]

def n_samples(path):
    """Retrieve the number of samples stored at the path"""
    with h5py.File(path, 'r') as f:
        group = f.get(CONFIG['event_key']) # from aict_tools.io.get_number_of_rows_in_table
        return group[next(iter(group.keys()))].shape[0]

# TODO also provide a download of crab's photon stream data
