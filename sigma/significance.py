import numpy as np
import pandas as pd
from fact.analysis import li_ma_significance
from pyirf.sensitivity import relative_sensitivity
from scipy.optimize import minimize, minimize_scalar
from sigma import CONFIG # import from this project

THETA_COLUMNS = [
    'theta_deg',
    'theta_deg_off_1',
    'theta_deg_off_2',
    'theta_deg_off_3',
    'theta_deg_off_4',
    'theta_deg_off_5',
]

def _n_on_and_off(events_df, prediction_threshold, theta2_cut, verbose):
    n_total = len(events_df)
    if prediction_threshold > 0:
        events_df = events_df.query(f'gamma_prediction >= {prediction_threshold}')
    n_pt = len(events_df) # number of events after applying the prediction_threshold

    # apply the theta cut and compute the number of on-events and off-events
    theta_cut = np.sqrt(theta2_cut)
    theta_on = events_df.theta_deg
    theta_off = pd.concat([
        events_df['theta_deg_off_{}'.format(i)]
        for i in range(1, 6)
    ])
    n_on = np.sum(theta_on < theta_cut)
    n_off = np.sum(theta_off < theta_cut)
    if verbose:
        print(", ".join([
            "prediction_threshold={}".format(prediction_threshold),
            "theta2_cut={}".format(theta2_cut),
            "n_total={}".format(n_total),
            "n_prediction_threshold={} ({:.2f}%)".format(n_pt, n_pt/n_total),
            "n_on={} ({:.4f}%)".format(n_on, n_on/n_pt),
            "n_off={} ({:.4f}%)".format(n_off, n_off/n_pt)
        ]))
    return n_on, n_off

def _sensitivity(events_df, prediction_threshold, theta2_cut, alpha, observation_time, verbose):
    n_on, n_off = _n_on_and_off(events_df, prediction_threshold, theta2_cut, verbose)
    return relative_sensitivity(n_on*50/observation_time, n_off*50/observation_time, alpha)

def _lima(events_df, prediction_threshold, theta2_cut, alpha, verbose):
    n_on, n_off = _n_on_and_off(events_df, prediction_threshold, theta2_cut, verbose)
    return li_ma_significance(n_on, n_off, alpha=alpha)

def _optimize_lima(events_df, theta2_cut, alpha, verbose, n_trials):
    def f(x, events_df, theta2_cut, alpha, verbose):
        return -_lima(events_df, x, theta2_cut, alpha, verbose)

    # minimize_scalar is the best approach if only one local minimum exists
    best_res = minimize_scalar(f, bounds=(0.0, 1.0), method='bounded', args=(events_df, theta2_cut, alpha, verbose))

    # for other cases, which indeed occur when the noisy classifier is particularly weak,
    # we need a minimization approach that can multi-start from random guesses
    for _ in range(n_trials-1):
        res = minimize(f, np.random.rand(), bounds=((0.0,1.0),), args=(events_df, theta2_cut, alpha, verbose))
        if res.success and (best_res is None or best_res.fun > res.fun):
            best_res = res

    return -best_res.fun, best_res.x # return the optimum Li&Ma value together with the prediction_threshold

def _optimize_lima_theta2(events_df, alpha, verbose):
    def f(x, events_df, alpha, verbose):
        return -_lima(events_df, x[0], x[1], alpha, verbose)
    res = minimize(
        f,
        [np.random.rand(), np.random.rand()*.2], # initial guess
        method = "Powell", # the other ones do not work properly without gradients
        bounds = [(0.0, 1.0), (0.0, 0.2)],
        args = (events_df, alpha, verbose)
    )
    return -res.fun, res.x[0], res.x[1]

def lima(events_df, prediction_threshold=None, theta2_cut=None, alpha=None, verbose=False, n_trials=100):
    """Compute the Li&Ma sigma value which measures the significance of detection."""
    if prediction_threshold is None:
        prediction_threshold = CONFIG['prediction_threshold']
    if theta2_cut is None:
        theta2_cut = CONFIG['theta2_cut']
    if alpha is None:
        alpha = CONFIG['alpha']
    if prediction_threshold == "optimize" and theta2_cut == "optimize":
        print("WARNING: joint optimization of prediction_threshold and theta2_cut does not yet multi-start")
        return _optimize_lima_theta2(events_df, alpha, verbose)
    elif prediction_threshold == "optimize":
        return _optimize_lima(events_df, theta2_cut, alpha, verbose, n_trials)
    else:
        return _lima(events_df, prediction_threshold, theta2_cut, alpha, verbose)

def sensitivity(events_df, prediction_threshold=None, theta2_cut=None, alpha=None, observation_time=None, verbose=False):
    """Compute the relative sensitivity normalized to 50h of observation time."""
    if prediction_threshold is None:
        prediction_threshold = CONFIG['prediction_threshold']
    if theta2_cut is None:
        theta2_cut = CONFIG['theta2_cut']
    if alpha is None:
        alpha = CONFIG['alpha']
    if observation_time is None:
        observation_time = CONFIG['observation_time']
    return _sensitivity(events_df, prediction_threshold, theta2_cut, alpha, observation_time, verbose)

def n_on_and_off(events_df, prediction_threshold=None, theta2_cut=None, verbose=False):
    """Compute (n_on, n_off), the numbers of gamma predictions in the On and Off regions."""
    if prediction_threshold is None:
        prediction_threshold = CONFIG['prediction_threshold']
    if theta2_cut is None:
        theta2_cut = CONFIG['theta2_cut']
    return _n_on_and_off(events_df, prediction_threshold, theta2_cut, verbose)
