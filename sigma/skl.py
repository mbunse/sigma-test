import numpy as np
from sigma import CONFIG, get_input, join_dl3, significance
from sigma.get_input import n_samples # re-export

def __sanitize_X(X):
    """Remove all samples with NaN values and clip float32 Infs."""
    X = X[~np.isnan(X).any(1)].astype(np.float32)
    X[np.isposinf(X)] = np.finfo('float32').max
    X[np.isneginf(X)] = np.finfo('float32').min
    return X

def get_training_data(gamma_dl2_path=None, hadron_dl2_path=None, sanitize=True, **kwargs):
    """Return a feature matrix X of shape (n_samples, n_features) and a label array y
    of shape (n_samples). The label array takes values in {0, 1} which represent hadronic
    and gamma particles."""
    if "only_gamma" in kwargs:
        raise ValueError("only_gamma is not supported. Use sigma.get_input.get_simulated_dl2 instead.")
    np_df = get_input \
      .get_simulated_dl2(gamma_dl2_path, hadron_dl2_path, **kwargs) \
      .to_numpy() # numpy array from pandas DataFrame
    X = np_df[:, :-1]
    y = np_df[:, -1].astype(int)
    if sanitize:
        y = y[~np.isnan(X).any(1)]
        X = __sanitize_X(X)
    return X, y

def get_crab_data(crab_dl2_path=None, sanitize=True, **kwargs):
    """Return a feature matrix X of shape (n_samples, n_features) that is to be
    predicted. Evaluate your predictions on this set with sigma.skl.lima_test"""
    X = get_input.get_crab_dl2(crab_dl2_path, **kwargs).to_numpy()
    if sanitize:
        X = __sanitize_X(X)
    return X

def _join_dl3(crab_gamma_probabilities, crab_dl2_path, crab_dl3_path, verbose):
    df = get_input.get_crab_dl2(crab_dl2_path, all_features=True)
    df = df[~np.isnan(df[CONFIG['features']].to_numpy()).any(1)] # select sanitized instances
    df = df[CONFIG['join_id']] # select ID columns
    df['gamma_prediction'] = crab_gamma_probabilities # add the probabilities
    return join_dl3.join_dl3(df, crab_dl3_path=crab_dl3_path, verbose=verbose)['events']

def lima_test(crab_gamma_probabilities, crab_dl2_path=None, crab_dl3_path=None, verbose=False, **kwargs):
    """Compute the Li&Ma sigma value which measures the significance of detection."""
    return significance.lima(
        _join_dl3(crab_gamma_probabilities, crab_dl2_path, crab_dl3_path, verbose),
        verbose=verbose,
        **kwargs
    )

def sensitivity(crab_gamma_probabilities, crab_dl2_path=None, crab_dl3_path=None, verbose=False, **kwargs):
    """Compute the relative sensitivity normalized to 50h of observation time."""
    return significance.sensitivity(
        _join_dl3(crab_gamma_probabilities, crab_dl2_path, crab_dl3_path, verbose),
        verbose=verbose,
        **kwargs
    )

def n_on_and_off(crab_gamma_probabilities, crab_dl2_path=None, crab_dl3_path=None, verbose=False, **kwargs):
    """Compute (n_on, n_off), the numbers of gamma predictions in the On and Off regions."""
    return significance.n_on_and_off(
        _join_dl3(crab_gamma_probabilities, crab_dl2_path, crab_dl3_path, verbose),
        verbose=verbose,
        **kwargs
    )
