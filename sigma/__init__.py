import yaml, os
__version__ = "0.1.3"


# read the default arguments
def __load_config(path):
    with open(path) as stream:
        config = yaml.safe_load(stream)
    for key, value in config.items():
        if isinstance(value, str):
            config[key] = value.replace("$(RESOURCE_DIR)", RESOURCE_DIR)
    return config

RESOURCE_DIR = os.path.dirname(__file__) + "/resources"
CONFIG = __load_config(RESOURCE_DIR + "/config.yml")


# import from this project (requires CONFIG to be read)
from sigma import join_dl3, significance


def lima(df, reconstructed_columns=None, crab_dl3_path=None, **kwargs):
    """Compute the Li&Ma sigma value for a custom reconstruction.

    Calling this function is equivalent to the following call:

        sigma.significance.lima(
            sigma.join_dl3.join_dl3(df, reconstructed_columns, crab_dl3_path)['events'],
            **kwargs
        )

    Please refer to the documentation of sigma.significance.lima and
    sigma.join_dl3.join_dl3 for more information.
    """
    return significance.lima(
    	join_dl3.join_dl3(df, reconstructed_columns, crab_dl3_path)['events'],
    	**kwargs
    )
