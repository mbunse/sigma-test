from unittest import TestCase

import numpy as np
import time
from sklearn.ensemble import RandomForestClassifier
from sigma import CONFIG, skl # import the sklearn-style API of this package
from sigma.join_dl3 import __get_crab_dl3
from sigma.significance import lima, sensitivity

class TestExample(TestCase):
    def test_example(self):
        print("Loading the data; doing so might take a while")
        X_trn, y_trn = skl.get_training_data(verbose=True)
        X_wrk = skl.get_crab_data(verbose=True)
        y_pred = np.random.uniform(size=X_wrk.shape[0])
        print("Optimizing the prediction_threshold with verbose=True")
        value, pt = skl.lima_test(y_pred, prediction_threshold="optimize", verbose=True)
        print("----------------------------------------------------------------------")
        print(f"Achieved {value} sigma with an optimized prediction_threshold={pt}")
        s = skl.sensitivity(y_pred, prediction_threshold=pt)
        n_on, n_off = skl.n_on_and_off(y_pred, prediction_threshold=pt)
        print(f"{value} sigma ≡ {s} sensitivity, n_on={n_on}, n_off={n_off}")
        print("----------------------------------------------------------------------\n")

def get_dl3():
    return __get_crab_dl3(CONFIG['crab_dl3'])

class TestOptimize(TestCase):
    def test_optimize_theta(self):
        print("Loading the DL3 data")
        df = get_dl3()
        print("Jointly optimizing the prediction_threshold and theta2_cut")
        value, pt, t2 = lima(
            df,
            prediction_threshold="optimize",
            theta2_cut="optimize",
            verbose=True
        )
        print(f"Achieved {value} sigma with prediction_threshold={pt} and theta2_cut={t2}")
    def __optimize_n_trials(self, n_trials):
        df = get_dl3()
        start = time.time()
        value, pt = lima(
            df,
            prediction_threshold="optimize",
            n_trials=n_trials,
            verbose=False
        )
        elapsed = time.time() - start
        print("----------------------------------------------------------------------")
        print(f"Achieved {value} sigma BEFORE UPDATING with prediction_threshold={pt}")
        value = lima(df, prediction_threshold=pt, verbose=True)
        s = sensitivity(df, prediction_threshold=pt, verbose=True)
        print(f"Achieved {value} sigma (≡ {s} sensitivity) in {elapsed} sec with n_trials={n_trials}")
        print("----------------------------------------------------------------------\n")
    def test_optimize_single_trial(self):
        self.__optimize_n_trials(1)
    def test_optimize_hundred_trials(self):
        self.__optimize_n_trials(100)
