# Sigma test for FACT

Easy-to-use Li&Ma sigma validation for prediction models with the FACT telescope.

Check out my four-minute introduction to telescopes like FACT: https://www.youtube.com/watch?v=mnuCUZMXvUM


## Setup

The package is installed with pip (remember to activate a suitable environment):

    pip3 install .


## Changelog

**v0.1.2:** Load diffuse training gammas instead of wobble-mode training gammas.


## Scikit-learn style usage

Let's learn to distinguish gamma particles (signal) from hadronic particles (noise), representing all data as feature matrices `X` and integer label arrays `y`. There are two data sets: The labeled simulated data and the unlabeled open data set of the crab nebula.

- The labeled data can be used for training a prediction model and for validating models with supervised metrics like cross-validated accuracy.
- The unlabeled data can be used with a use-case specific metric: The Li&Ma significance of detection test. The higher its value, the more certain is the telescope about actually having seen the crab nebula. Since we know that the telescope was pointed at this celestial object, the significance value should be as high as possible.

Let's go through the process. In this example, `X_trn` is a feature matrix of the 16 numeric features that are used in standard FACT analyses.

    from sigma import skl # import the sklearn-style API of this package
    
    # 1) Read the simulated training data.
    #
    # CAUTION: This function call will trigger a 5GB download, but only on the first call.
    # the retrieved file will remain stored on your hard disk.
    #
    X_trn, y_trn = skl.get_training_data()
    
    
    # 2) You can now train a model that predicts y from X and test it with a standard
    #    metric like cross-validated accuracy. For simplicity, we will skip this step here.
    
    
    # 3) Read the working set that we want to predict, i.e. the crab nebula data
    #
    # CAUTION: This call will trigger another 2GB download.
    #
    X_wrk = skl.get_crab_data()
    
    
    # 4) You can now apply your model to X_wrk. In particular, the Li&Ma test that we'll
    #    take out requires scores/probabilities for class '1' for each example in X_wrk.
    #    For now, we will simply use a random vector of scores for this purpose.
    #
    import numpy as np
    y_pred = np.random.uniform(size=X_wrk.shape[0]) # same length
    
    
    # 5) Take out the Li&Ma test. Random predictions should produce values around 5,
    #    good predictions should achieve around 20 (higher is better).
    #
    # CAUTION: This call triggers a 150MB download.
    #
    # NOTE: You can pass the argument `prediction_threshold="optimize"` to optimize
    # the sigma value over the prediction threshold automatically.
    #
    skl.lima_test(y_pred)
    
    
    # 6) NEW: You can now compute additional metrics
    #
    skl.sensitivity(y_pred) # lower is better
    skl.n_on_and_off(y_pred)


## Tuning the prediction threshold

The Li&Ma test functions can tune the confidence score above which an event is considered to picture a gamma.

    skl.lima_test(y_pred, prediction_threshold="optimize")

By default, this optimization is taken out in 100 trials, which can take about 5 sec on the open crab data set. If this time is too long, you can reduce the number of trials via the `n_trials` parameter of all lima test functions. Our experience is that the default DL3 predictions work well with `n_trials=1` but that other predictions will require many trials to achieve high Li&Ma scores.


## Usage with CSV prediction files

You compute the sigma value of Li&Ma from a file with prediction and ID columns. An example:

    import sigma, pandas
    df = pandas.read_csv('input.csv', sep=',', names=['run_id', 'event_num', 'night', 'gamma_prediction'])
    sigma.lima(df)

The first call of `sigma.lima` will download the crab nebula DL3 file (ca. 130 MB) and store it for future use. If you already have it somewhere, you can also provide its path as a keyword argument. Other keyword arguments specify the meta-parameters of this test, e.g.:

    sigma.lima(df, crab_dl3_path='path/to/existing/dl3.hdf5', prediction_threshold=0.6)

You can not only replace `'gamma_prediction'` in the standard DL3 reconstruction - any other prediction can be replaced just similarly, e.g. `'gamma_energy_prediction'` or directional reconstructions. Which DL3 properties are replaced simply depends on the columns that are provided in the DataFrame above.

More information can be found in the documentation of the package functions, e.g.:

    help(sigma.lima)


## Advanced usage: configuration

All default arguments are configured in `sigma/resources/config.yml`, which is loaded into `sigma.CONFIG` when you import the sigma module or any of its sub-modules. If you want to make several changes, e.g. change all paths of DL2 and DL3 files, you can replace the corresponding keys in `sigma.CONFIG`.

    import sigma
    sigma.CONFIG['crab_dl3'] = '/whatever/your/path/is.hdf5'


## Development: starting the unit tests

Test the package with the `unittest` python module:

    venv/bin/pip install .[test]
    venv/bin/python -m unittest
