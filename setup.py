import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="sigma-test",
    version="0.1.3",
    author="Mirko Bunse",
    author_email="mirko.bunse@tu-dortmund.de",
    description="Easy-to-use Li&Ma sigma validation for prediction models with FACT",
    long_description=long_description,
    url="https://bitbucket.org/mbunse/sigma-test",
    packages=setuptools.find_packages(),
    classifiers=[
        "Operating System :: POSIX :: Linux",
        "Intended Audience :: Science/Research",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.6",
        "License :: OSI Approved :: MIT License",
    ],
    install_requires = [
        'pyfact>=0.19.1',
        'numpy',
        'pandas',
        'pyirf',
        'pyyaml'
    ],
    python_requires='>=3.6',
    include_package_data=True,
    test_suite='nose.collector',
    extras_require = {
        'test' : ['nose', 'sklearn']
    }
)
